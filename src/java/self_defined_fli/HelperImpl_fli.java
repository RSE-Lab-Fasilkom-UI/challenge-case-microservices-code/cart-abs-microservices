// Name this file "ManualFLIExample.java" will be ignored by the build process
package ABS.Framework.Helper;

import abs.backend.java.lib.types.ABSString;
import abs.backend.java.lib.types.ABSInteger;
import abs.backend.java.lib.types.ABSUnit;
import abs.backend.java.lib.runtime.FLIHelper;

import com.rse.middleware.DataTransformer;
import ABS.StdLib.Pair;

import java.lang.reflect.Method;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;
import java.util.UUID;
import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.digest.DigestUtils;
import org.json.JSONObject;
import org.json.JSONException;

public class HelperImpl_fli extends HelperImpl_c {

    @Override
    public ABSString fli_hash(ABSString input) {
    	String result = DigestUtils.sha1Hex(input.getString());
        return ABSString.fromString(result);
    }
    @Override
    public ABSString fli_timestamp() {
    	String result = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss.SSSSSS").format(new Date());
    	return ABSString.fromString(result);
    }
    @Override
    public ABSString fli_datenow() {
        String result = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        return ABSString.fromString(result);
    }
    @Override
    public ABSString fli_dateonlynow() {
        String result = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        return ABSString.fromString(result);
    }
    @Override
    public ABSUnit fli_echo(ABSString input) {
        System.out.println(input.getString());
        return ABSUnit.UNIT;
    }
    @Override
    public ABS.StdLib.List<ABSString> fli_parse_json_to_list(ABSString json) {
        String json_java = json.getString();
        try {
            return DataTransformer.convertJSONToABSList(json_java);
        } catch (JSONException e) {
            return new ABS.StdLib.List_Nil<ABSString>();
        }
    }
    @Override
    public ABS.StdLib.Map<ABSString, ABSString> fli_parse_json_to_map(ABSString json) {
        String json_java = json.getString();
        try {
            return DataTransformer.convertJSONToABSMap(json_java);
        } catch (JSONException e) {
            return new ABS.StdLib.Map_EmptyMap<ABSString, ABSString>();
        }
    }
    @Override
    public ABSString fli_uuid() {
        String result = UUID.randomUUID().toString();
        return ABSString.fromString(result);
    }
    @Override
    public ABSString fli_base64_encode(ABSString input) {
    	String result = Base64.getEncoder().encodeToString(input.getString().getBytes());
        return ABSString.fromString(result);
    }
    @Override
    public ABSString fli_encrypt(ABSString valueToEnc, ABSString encKey) {
        String ALGORITHM = "AES";
        try {
            Key key = new SecretKeySpec(encKey.getString().getBytes(), ALGORITHM);
            Cipher c = Cipher.getInstance(ALGORITHM);
            c.init(Cipher.ENCRYPT_MODE, key);
            byte[] encValue = c.doFinal(valueToEnc.getString().getBytes());
            String result = Base64.getEncoder().encodeToString(encValue);
            return ABSString.fromString(result);
        } catch (Exception e) {
            return ABSString.fromString("");
        }
    }
    @Override
    public ABSString fli_properties_env(ABSString propertiesFileName, ABSString key) {
        FileInputStream input = null;
        Properties prop = new Properties();
        ABSString result = ABSString.fromString("");

        try {
            input = new FileInputStream(propertiesFileName.getString());
            // load a properties file
            prop.load(input);
            result = ABSString.fromString(prop.getProperty(key.getString()).trim());
        } catch (IOException ex) {
            System.out.println(ex);
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                }
            }
        }
        return result;
    }
}
