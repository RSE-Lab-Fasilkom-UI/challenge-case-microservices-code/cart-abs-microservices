INSERT INTO public.cartimpl(idCartItemList, id) VALUES ('1,3', 1);
INSERT INTO public.cartimpl(idCartItemList, id) VALUES ('2,4', 2);

INSERT INTO public.cartitemimpl(quantity, price, idCatalogItem, id) VALUES (2, 2000, 1, 1);
INSERT INTO public.cartitemimpl(quantity, price, idCatalogItem, id) VALUES (2, 4000, 2, 2);
INSERT INTO public.cartitemimpl(quantity, price, idCatalogItem, id) VALUES (2, 6000, 3, 3);
INSERT INTO public.cartitemimpl(quantity, price, idCatalogItem, id) VALUES (2, 8000, 4, 4);

SELECT pg_catalog.setval('public.cartimpl_id_seq', 2, true);
SELECT pg_catalog.setval('public.cartitemimpl_id_seq', 4, true);
