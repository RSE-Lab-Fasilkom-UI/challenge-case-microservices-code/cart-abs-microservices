module ABS.Framework.ABSConnector;
export *;

import APIAdaptor, APIAdaptorImpl from ABS.Framework.APIAdaptor;
import Helper, HelperImpl from ABS.Framework.Helper;

interface ABSConnector {
    List<Map<String, String>> getData(String endpointAddress, Map<Int, Pair<String, String>> payload);
    Bool isItemAvailable(List<Map<String, String>> objectList);
}

class ABSConnectorImpl implements ABSConnector {
    List<Map<String, String>> getData(String endpointAddress, Map<Int, Pair<String, String>> payload) {
        APIAdaptor adaptor = new local APIAdaptorImpl();
        Helper helper = new local HelperImpl();
        String endpoint = helper.properties_env("services.properties", "nginx");
        endpoint = endpoint + endpointAddress;
        payload = insert(payload, Pair(-1, Pair("ENDPOINT", endpoint)));

        Map<String, String> inquiry_response = adaptor.call_reqres_POST(payload);
        String jsonData = lookupUnsafe(inquiry_response, "data");

        List<Map<String, String>> result = Nil;
        String firstChar = substr(jsonData, 0, 1);
        if (firstChar == "[") {
            List<String> entryList = helper.parse_json_to_list(jsonData);
            Int size = length(entryList);
            Int count = 0;

            while (count < size) {
                String entryJson = nth(entryList, count);
                Map<String, String> entry = helper.parse_json_to_map(entryJson);
                result = Cons(entry, result);
            }
        } else {
            Map<String, String> entry = helper.parse_json_to_map(jsonData);
            result = Cons(entry, result);
        }
        return result;
    }

    Bool isItemAvailable(List<Map<String, String>> objectList) {
        Bool result = !isEmpty(objectList);
        if (result) {
            Map<String, String> firstItem = nth(objectList, 0);
            String idStr = lookupUnsafe(firstItem, "id");
            result = result && (idStr != "null");
        }
        return result;
    }
}
